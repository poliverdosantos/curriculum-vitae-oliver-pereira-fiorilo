import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AgendaComponent } from './components/agenda/agenda.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { AgendaElectronicaComponent } from './components/agenda-electronica/agenda-electronica.component';
import { FormularioContactoComponent } from './components/formulario-contacto/formulario-contacto.component';
import { FormsModule , } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { NavigationComponent } from './components/navigation/navigation.component';
import { TaskFormComponent } from './components/task-form/task-form.component';
import { TaskComponent } from './components/task/task.component';
import { TaskListComponent } from './components/task-list/task-list.component';
import { TaskService } from './services/task.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// angular materaial
import {MatFormFieldModule} from '@angular/material/form-field';
@NgModule({
  declarations: [
    AppComponent,
    AgendaComponent,
    NavbarComponent,
    AgendaElectronicaComponent,
    FormularioContactoComponent,
    NavigationComponent,
    TaskFormComponent,
    TaskComponent,
    TaskListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule
    ,MatFormFieldModule
  ],
  providers: [TaskService],
  bootstrap: [AppComponent]
})
export class AppModule { }
